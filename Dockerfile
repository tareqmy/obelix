FROM openjdk:11-jre-slim
LABEL creator="Tareq Mohammad Yousuf"
LABEL email="tareq.y@gmail.com"

RUN mkdir -p /obelix/data
WORKDIR /obelix

COPY build/libs/obelix-1.0.0.jar /obelix/

EXPOSE 8090

ENTRYPOINT ["sh","-c","java -jar obelix-1.0.0.jar"]
