package com.telefonix.obelix.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tareqmy on 2019-05-19.
 */
@Component
@ConfigurationProperties("obelix")
public class ObelixProperties {

    private final List<String> allowedPaths = new ArrayList<>();

    public List<String> getAllowedPaths() {
        return allowedPaths;
    }
}
