package com.telefonix.obelix.service;

import com.telefonix.obelix.config.ObelixProperties;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-05-14.
 */
@Service
public class FileManagerServiceImpl implements FileManagerService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private List<String> allowedPaths;

    @Autowired
    public FileManagerServiceImpl(ObelixProperties obelixProperties) {
        allowedPaths = obelixProperties.getAllowedPaths();
    }

    @Override
    public boolean exist(String path, String fileName) {
        validateArguments(path, fileName);
        return Files.exists(Paths.get(path, fileName));
    }

    @Override
    public void upload(String path, String fileName, MultipartFile file) {
        validateArguments(path, fileName);

        byte[] bytes;
        try {
            bytes = file.getBytes();
            Files.write(Paths.get(path, fileName), bytes);
        } catch (IOException e) {
            log.error("failed to write {}", e.getMessage());
        }
    }

    @Override
    public byte[] download(String path, String fileName) {
        validateArguments(path, fileName);

        try {
            return Files.readAllBytes(Paths.get(path, fileName));
        } catch (IOException e) {
            log.error("failed to read {}", e.getMessage());
        }
        return null;
    }

    @Override
    public void delete(String path, String fileName) {
        validateArguments(path, fileName);

        try {
            Files.deleteIfExists(Paths.get(path, fileName));
        } catch (IOException e) {
            log.error("failed to delete {}", e.getMessage());
        }
    }

    @Override
    public FileDetails getFile(String path, String fileName) {
        validateArguments(path, fileName);
        return new FileDetails(Paths.get(path, fileName));
    }

    @Override
    public List<FileDetails> getFiles(String path) {
        validatePath(path);

        if (!Files.exists(Paths.get(path)) || !Files.isDirectory(Paths.get(path))) {
            throw new RuntimeException("Not a path!");
        }

        List<FileDetails> collect = new ArrayList<>();
        try {
            collect = Files.list(Paths.get(path))
                .filter(p -> !Files.isDirectory(p))
                .map(FileDetails::new)
                .collect(Collectors.toList());
        } catch (IOException e) {
            log.error("failed to get info {}", e.getMessage());
        }
        return collect;
    }

    @Override
    public boolean move(String pathFrom, String fromFileName, String pathTo, String toFileName) {
        validateArguments(pathFrom, fromFileName);
        validateArguments(pathTo, toFileName);
        try {
            Files.move(Paths.get(pathFrom, fromFileName), Paths.get(pathTo, toFileName), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            log.error("failed to move {}", e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public boolean copy(String pathFrom, String fromFileName, String pathTo, String toFileName) {
        validateArguments(pathFrom, fromFileName);
        validateArguments(pathTo, toFileName);
        try {
            Files.copy(Paths.get(pathFrom, fromFileName), Paths.get(pathTo, toFileName), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            log.error("failed to copy {}", e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public void make(String path, String fileName) {
        validatePath(path);

        try {
            Files.createDirectories(Paths.get(path, fileName));
        } catch (IOException e) {
            log.error("failed to make path {}", e.getMessage());
        }
    }

    private void validatePath(String path) {
        if (allowedPaths.stream().filter(path::startsWith).count() <= 0) {
            throw new RuntimeException("Invalid arguments!");
        }
    }

    private void validateArguments(String path, String fileName) {
        validatePath(path);

        if (StringUtils.isEmpty(fileName)
            || StringUtils.isEmpty(path)) {
            throw new RuntimeException("Invalid arguments!");
        }
    }
}
