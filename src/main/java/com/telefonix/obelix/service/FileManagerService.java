package com.telefonix.obelix.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by tareqmy on 2019-05-14.
 */
public interface FileManagerService {

    //0. file exists
    boolean exist(String path, String fileName);

    //1. add/upload file
    void upload(String path, String fileName, MultipartFile file);

    //2. get/download file
    byte[] download(String path, String fileName);

    //3. remove/delete file
    void delete(String path, String fileName);

    //4. get file details
    FileDetails getFile(String path, String fileName);

    //5. get all file details in path
    List<FileDetails> getFiles(String path);

    //6. move file
    boolean move(String pathFrom, String fromFileName, String pathTo, String toFileName);

    //7. move file
    boolean copy(String pathFrom, String fromFileName, String pathTo, String toFileName);

    //8. make path
    void make(String path, String fileName);
}
