package com.telefonix.obelix.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created by tareqmy on 2019-05-15.
 */
public class FileDetails {

    private String directory;

    private String fileName;

    private long size;

    public FileDetails() {
    }

    public FileDetails(Path path) {
        setDirectory(path.toAbsolutePath().toString());
        setFileName(path.getFileName().toString());
        try {
            setSize(Files.size(path));
        } catch (IOException e) {
            setSize(0);
        }
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "FileDetails{" +
            "directory='" + directory + '\'' +
            ", fileName='" + fileName + '\'' +
            ", size=" + size +
            '}';
    }
}
