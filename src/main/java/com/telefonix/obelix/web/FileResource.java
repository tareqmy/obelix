package com.telefonix.obelix.web;

import com.telefonix.obelix.service.FileDetails;
import com.telefonix.obelix.service.FileManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by tareqmy on 2019-05-14.
 */
@RestController
@RequestMapping("/api")
public class FileResource {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private FileManagerService fileManagerService;

    //0. file exists
    @RequestMapping(value = "/files/exists",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> exists(@RequestParam("path") String path,
                                       @RequestParam("fileName") String fileName) {
        log.info("Enter: exists {}/{}", path, fileName);
        boolean exist = fileManagerService.exist(path, fileName);
        if (exist) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }

    //1. add/upload file
    @RequestMapping(value = "/files/upload",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Void> uploadFile(@RequestParam("path") String path,
                                           @RequestParam("fileName") String fileName,
                                           @RequestParam("file") @NotNull MultipartFile file) {
        log.info("Enter: uploadFile {}/{}", path, fileName);
        fileManagerService.upload(path, fileName, file);
        return ResponseEntity.ok().build();
    }

    //2. get/download file
    @RequestMapping(value = "/files/download",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<byte[]> downloadFile(@RequestParam("path") String path,
                                               @RequestParam("fileName") String fileName) {
        log.info("Enter: downloadFile {}/{}", path, fileName);
        byte[] download = fileManagerService.download(path, fileName);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentLength(download.length);
        headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
        headers.set(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        return new ResponseEntity<>(download, headers, HttpStatus.OK);
    }

    //3. remove/delete file
    @RequestMapping(value = "/files/delete",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> delete(@RequestParam("path") String path,
                                       @RequestParam("fileName") String fileName) {
        log.info("Enter: delete {}/{}", path, fileName);
        fileManagerService.delete(path, fileName);
        return ResponseEntity.ok().build();
    }

    //4. get file details
    @RequestMapping(value = "/files/{fileName}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public FileDetails getFile(@RequestParam("path") String path,
                               @PathVariable String fileName) {
        log.info("Enter: getFile {}/{}", path, fileName);
        return fileManagerService.getFile(path, fileName);
    }

    //5. get all file details in path
    @RequestMapping(value = "/files",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FileDetails> getFiles(@RequestParam("path") String path) {
        log.info("Enter: getFiles {}", path);
        return fileManagerService.getFiles(path);
    }

    //6. move file
    @RequestMapping(value = "/files/move",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> move(@RequestParam("pathFrom") String pathFrom,
                                     @RequestParam("fromFileName") String fromFileName,
                                     @RequestParam("pathTo") String pathTo,
                                     @RequestParam("toFileName") String toFileName) {
        log.info("Enter: move {}/{} > {}/{}", pathFrom, fromFileName, pathTo, toFileName);
        boolean move = fileManagerService.move(pathFrom, fromFileName, pathTo, toFileName);
        return ResponseEntity.ok().build();
    }

    //7. move file
    @RequestMapping(value = "/files/copy",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> copy(@RequestParam("pathFrom") String pathFrom,
                                     @RequestParam("fromFileName") String fromFileName,
                                     @RequestParam("pathTo") String pathTo,
                                     @RequestParam("toFileName") String toFileName) {
        log.info("Enter: copy {}/{} > {}/{}", pathFrom, fromFileName, pathTo, toFileName);
        boolean copy = fileManagerService.copy(pathFrom, fromFileName, pathTo, toFileName);
        return ResponseEntity.ok().build();
    }

    //8. make path
    @RequestMapping(value = "/files/make/directory",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> make(@RequestParam("path") String path,
                                     @RequestParam("fileName") String fileName) {
        log.info("Enter: make {}/{}", path, fileName);
        fileManagerService.make(path, fileName);
        return ResponseEntity.ok().build();
    }
}
